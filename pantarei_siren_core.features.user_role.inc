<?php

/**
 * Implementation of hook_user_default_roles().
 */
function pantarei_siren_core_user_default_roles() {
  $roles = array();

  // Exported role: administrator
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => '3',
  );

  // Exported role: power user
  $roles['power user'] = array(
    'name' => 'power user',
    'weight' => '2',
  );

  return $roles;
}
