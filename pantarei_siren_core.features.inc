<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function pantarei_siren_core_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}
