<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function pantarei_siren_core_user_default_permissions() {
  $permissions = array();

  // Exported permission: search content
  $permissions['search content'] = array(
    'name' => 'search content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
    ),
  );

  // Exported permission: switch users
  $permissions['switch users'] = array(
    'name' => 'switch users',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
    ),
  );

  // Exported permission: use text format filtered_html
  $permissions['use text format filtered_html'] = array(
    'name' => 'use text format filtered_html',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'authenticated user',
      '2' => 'power user',
    ),
  );

  // Exported permission: use text format full_html
  $permissions['use text format full_html'] = array(
    'name' => 'use text format full_html',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'power user',
    ),
  );

  // Exported permission: use text format php_code
  $permissions['use text format php_code'] = array(
    'name' => 'use text format php_code',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: use text format purified_html
  $permissions['use text format purified_html'] = array(
    'name' => 'use text format purified_html',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'power user',
    ),
  );

  // Exported permission: view post access counter
  $permissions['view post access counter'] = array(
    'name' => 'view post access counter',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
    ),
  );

  return $permissions;
}
